package com.tracfone.universalentry;

public class TestConnector {

	public void printLogInfo(String logString) {
		System.out.println("This is being logged from this class: " + logString);
	}
	
	public static void main(String args[]) {
		TestConnector myLogger = new TestConnector();
		
		myLogger.printLogInfo("i need to see this in the console");
		
	}

}
